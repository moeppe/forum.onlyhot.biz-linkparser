﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace forum_onlyhot_biz_linkparser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.ReadOnly = true;
            textBox2.ReadOnly = true;
            string[] html = textBox1.Text.Split(new string[]{"http://forum.onlyhot.net/redirect"}, StringSplitOptions.RemoveEmptyEntries);
            Queue<string> queue = new Queue<string>();
            textBox1.Text = "";
            textBox2.Text = "";
            if (html.Length > 1)
            {
                for (int i = 1; i < html.Length; i++)
                {
                    queue.Enqueue("http://forum.onlyhot.net/redirect" + html[i].Substring(0, html[i].IndexOf('"')));
                    textBox1.Text += "http://forum.onlyhot.net/redirect" + html[i].Substring(0, html[i].IndexOf('"')) +
                                     "\r\n";
                }
            }
            WebClientEx c = new WebClientEx(new CookieContainer());
            while (queue.Count > 0)
            {
                string link = queue.Dequeue();
                string result = c.DownloadString(link);
                NameValueCollection nvc = new NameValueCollection();
                result = result.Substring(result.IndexOf("<form class=\"frm-form\""));
                result = result.Substring(0, result.IndexOf("</form>"));
                string[] inputs = result.Split(new string[] {"<input type=\"hidden\""},
                    StringSplitOptions.RemoveEmptyEntries);
                if (inputs.Length > 1)
                {
                    for (int j = 1; j < inputs.Length; j++)
                    {
                        string name = inputs[j].Substring(inputs[j].IndexOf("name=\"") + 6);
                        name = name.Substring(0, name.IndexOf('"'));
                        string value = inputs[j].Substring(inputs[j].IndexOf("value=\"") + 7);
                        value = value.Substring(0, value.IndexOf('"'));
                        nvc.Add(name, value);
                    }
                }
                byte[] response = c.UploadValues("http://forum.onlyhot.net/redirect_confirm.html", "POST", nvc);
                string result2 = System.Text.Encoding.UTF8.GetString(response);
                result2 =
                    result2.Substring(result2.IndexOf("<meta http-equiv=\"refresh\" content=\"1;URL=") +
                                      "<meta http-equiv=\"refresh\" content=\"1;URL=".Length);
                result2 = result2.Substring(0, result2.IndexOf('"'));
                result2 = result2.Substring(result2.IndexOf("/url/") + 5);
                textBox2.Text += result2 + "\r\n";
                Application.DoEvents();
            }
            textBox1.ReadOnly = textBox2.ReadOnly = false;
        }

    }
    public class WebClientEx : WebClient
    {
        public WebClientEx(CookieContainer container)
        {
            this.container = container;
        }

        private readonly CookieContainer container = new CookieContainer();

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest r = base.GetWebRequest(address);
            var request = r as HttpWebRequest;
            if (request != null)
            {
                request.CookieContainer = container;
            }
            return r;
        }

        protected override WebResponse GetWebResponse(WebRequest request, IAsyncResult result)
        {
            WebResponse response = base.GetWebResponse(request, result);
            ReadCookies(response);
            return response;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            ReadCookies(response);
            return response;
        }

        private void ReadCookies(WebResponse r)
        {
            var response = r as HttpWebResponse;
            if (response != null)
            {
                CookieCollection cookies = response.Cookies;
                container.Add(cookies);
            }
        }
    }
}
