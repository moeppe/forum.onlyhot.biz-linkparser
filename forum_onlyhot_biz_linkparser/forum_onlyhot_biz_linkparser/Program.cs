﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;

namespace forum_onlyhot_biz_linkparser
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var IEVAlue = 9999; // can be: 9999 , 9000, 8888, 8000, 7000
            var targetApplication = Application.ExecutablePath.Split('\\').Last();
            var localMachine = Registry.LocalMachine;
            var parentKeyLocation = @"SOFTWARE\Microsoft\Internet Explorer\MAIN\FeatureControl";
            var keyName = "FEATURE_BROWSER_EMULATION";
            var subKey = localMachine.OpenSubKey(parentKeyLocation, true);
            subKey = subKey.OpenSubKey(keyName, true);
            subKey.SetValue(targetApplication, IEVAlue, RegistryValueKind.DWord);
            Application.Run(new Form1());
        }
    }
}
